<?php

/**
 * @file
 * Performs the actions for the module.
 */
class NodeLanguageCopy {

  private $contentType;
  private $languages;
  private $defaultAll;

  /**
   * Constructor: Initializes the list of available languages.
   */
  public function __construct() {

    $this->languages = locale_language_list();
    //dpm($languages);
  }

  /**
   * Sets the content type.
   */
  public function setContentType($contentType) {

    $this->contentType = $contentType;

    $storedValues = variable_get('node_language_copy', array());
    $this->defaultAll = in_array($contentType, $storedValues);
  }

  /**
   * Performs the alteration of the form:
   * - new tab to select the languages
   * - adds an additional submit action when the node is created
   */
  public function alterForm(&$form) {

    $path = drupal_get_path('module', 'node_language_copy');

    $form['node_language_copy'] = array(
      '#type' => 'fieldset',
      '#title' => t('Node copy languages'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
      '#attributes' => array(
        'class' => array('node-language-copy-form'),
      ),
      '#attached' => array(
        'js' => array($path . '/node_language_copy.js'),
        'css' => array($path . '/node_language_copy.css'),
      ),
      '#weight' => 40,
      'default' => $this->getDefault(),
      'languages' => $this->getCheckboxes(),
    );

    $form['actions']['submit']['#submit'][] = 'node_language_copy_form_submit';
    //dpm($form);
  }

  /**
   * Text to display about the default for this content type.
   */
  private function getDefault() {

    if ($this->defaultAll) {
      $text = t('All languages are selected by default.');
      $value = t('All languages');
    }
    else {
      $text = t('No language is selected by default.');
      $value = t('No language');
    }

    return array(
      'node-language-info' => array(
        '#markup' => '<div class="node-language-copy-info">' . $text . '</div>',
      ),
      'node-language-hidden' => array(
        '#type' => 'hidden',
        '#value' => $value,
        '#attributes' => array(
          'id' => 'node-language-copy-default',
        ),
      )
    );
  }

  /**
   * Returns the render array for the checkboxes for the available languages.
   */
  private function getCheckboxes() {

    if ($this->defaultAll) {
      $defaults = array_keys($this->languages);
    }
    else {
      $defaults = array();
    }

    return array(
      '#type' => 'checkboxes',
      '#options' => $this->languages,
      '#title' => t('Select the languages for which the node is to be copied.'),
      '#default_value' => $defaults,
    );
  }

  /**
   * Creates the copies of the node.
   *
   * We make use of the values that are already set by Drupal. They are set
   * in "$form_state['values']" , e.g.
   *
   * nid
   * type
   * language
   * languages = array(
   *   'de' => 'de',
   *   'en' => 0
   * )
   */
  public function copy(&$form_data) {

    $languages = array();
    foreach ($form_data['values']['languages'] as $language => $value) {
      if ($value) {
        $languages[] = $language;
      }
    }
    if (empty($languages)) {
      // Nothing to do
      return;
    }

    $nid = $form_data['nid'];
    $node = node_load($nid);
    if (!$node) {
      drupal_set_message(t('Internal error: node with nid @nid not found', array('@nid' => $nid)), 'error');
      return;
    }
    //dpm($node);

    // How to react if language "undefined" was chosen during the node creation?
    if ($node->language == LANGUAGE_NONE) {
      drupal_set_message(t('Node created with undefined language. No language copies will be generated.'), 'warning');
      return;
    }

    // Are there already nodes existing for another language?
    if ($this->existCopies($node)) {
      return;
    }

    $copies = 0;
    $nodeLanguage = $node->language;
    foreach ($languages as $language) {
      if ($language != $nodeLanguage) {
        $this->copyNode($node, $language);
        $copies++;
      }
    }

    if ($copies) {
      $node->tnid = $node->nid;
      node_save($node);
    }
  }

  /**
   * Checks if there is already a node for another language.
   * In this case we show a message and do nothing.
   */
  private function existCopies(&$node) {

    if (!$node->tnid) {
      return;
    }

    $translations = translation_node_get_translations($node->tnid);
    //dpm($translations);
    foreach ($translations as $language => $partlyNode) {
      if ($partlyNode->nid == $node->nid) {
        // The own node must be listed, too.
        continue;
      }
      drupal_set_message(t('Cannot create language copies for this node. There is already a node of language "@lang".',
        array('@lang' => $language)), 'warning');
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Copies the node with the given language.
   *
   * Attention: "$origNode" is a class, therefore given as a reference. Do
   * not modify!
   */
  private function copyNode($origNode, $language) {

    $node = clone($origNode);
    //$node->tnid = $origNode->nid;
    $node->language = $language;

    $newNid = replicate_entity('node', $node);
    if ($newNid) {
      // "tnid" wird von "replicate_entity" ignoriert. Müssen wir selbst setzen.
      $node = node_load($newNid);
      $node->tnid = $origNode->nid;
      $node->translate = TRUE;

      // Wer noch zusätzliche Aktionen durchführen möchte.
      // Hook: hook_node_language_copy_alter
      drupal_alter('node_language_copy', $node, $language);

      node_save($node);

      drupal_set_message(t('Language copy for language "@lang" created.',
        array('@lang' => $language)), 'status');
    }
    else {
      drupal_set_message(t('Failed to create language copy for this node for language "@lang".',
        array('@lang' => $language)), 'warning');
    }
  }
}
