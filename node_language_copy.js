/**
 * @file
 * Creates an info line in the "Node copy languages" tab.
 *
 * The code has been adapted from "modules/node/node.js".
 */
Drupal.behaviors.nodeLanguageCopy = {
  attach: function (context) {
    jQuery("fieldset.node-language-copy-form", context).drupalSetSummary(function (context) {
      return jQuery("#node-language-copy-default", context).val();
    });
  }
};
