**Description:
The "Node Language Copy" helps users to create content in a multi language
environment.

If a node for a language for some content type is created, the user may choose
to create clones of this node for all available languages or for selected
languages. These copies are marked as "needs translation".

This facilitates the task of the translators.

By default no languages are preselected. The content administrator may modifiy
this bevavior so that all available languages are preselected. In any case
the node creator may modify this selection.

The module has been tested with node and user references. Field collections will
also be cloned if the module "replicate_field_collection" is defined as a
dependency.

**Dependencies:
- translation
- locale
- replicate
- replicate_field_collection

